import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map, filter, catchError, mergeMap } from 'rxjs/operators';
import { WebsocketService } from './websocket.service';
import { environment } from '../../environments/environment';
import { ServerMessage } from '../../models/serverMessage';

export interface Message {
  author: string,
  message : string
}

@Injectable({
  providedIn: 'root'
})
export class BidService {

  messages: Subject<any>;
  
  // Our constructor calls our wsService connect method
  constructor(private wsService: WebsocketService) {
    this.messages = <Subject<any>>wsService
      .connect()
      .pipe(map((response: any): any => {
        return response;
      }))
   }
  
  // Our simplified interface for sending
  // messages back to our socket.io server
  sendMessage(msg:ServerMessage) {
    this.messages.next(msg);
  }

  closeSocket() {
    this.wsService.disconnect();
  }
}
