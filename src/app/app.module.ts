import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { WebsocketService } from './services/websocket.service';
import { BidService } from './services/bid.service';
import { UserAreaComponent } from './components/user-area/user-area.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    UserAreaComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [
    WebsocketService,
    BidService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
