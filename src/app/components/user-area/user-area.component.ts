import { Component, OnInit, Output, Input } from '@angular/core';
import { ElementRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { BidService } from '../../services/bid.service';
import { ServerMessage, MessageType } from '../../../models/serverMessage';


@Component({
  selector: 'app-user-area',
  templateUrl: './user-area.component.html',
  styleUrls: ['./user-area.component.css']
})
export class UserAreaComponent implements OnInit {
  @Input() id: number;
  @Input() users;
  @Input() user;
  price : string;

  constructor(private bidService: BidService) {
  }

  ngOnInit() {
  }


  getUserName():string {
    if(typeof this.users[this.id] != 'undefined') {
        return this.users[this.id].username;
    }else {
      return "";
    }
  }

  numberOnly(event) {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode == 46)
        return true
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;

  }

  isDisabled()
  {
    if(typeof this.users[this.id] != 'undefined' && this.users[this.id].username === this.user) {
      return true;
    }else {
      return false;
    }
  }

  giveBid() {
    if(this.price.length < 1) {
      alert('Please enter price');
      return;
    }
    let serverMessage:ServerMessage = {
      messageType : MessageType.BID,
      message : {
        username : this.user,
        bid: this.price
      }
    }
    this.bidService.sendMessage(serverMessage); 
  }
}


