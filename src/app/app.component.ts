import { Component } from '@angular/core';
import { BidService } from './services/bid.service';
import { UserAreaComponent } from './components/user-area/user-area.component';
import { ServerMessage, MessageType } from '../models/serverMessage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

    username: string = "";
    isJoined : boolean = false;
    userObject = [];
    bids = [];
    highestBid = [];

    constructor(private bidService: BidService) {
    }

    ngOnInit() {
      this.bidService.messages.subscribe(received => {
        this.messageReceived(received.msg);
      })
    }
    
    userJoin() {
      if(this.username.length < 1) {
        alert('Please enter username');
        return;
      }
      let serverMessage:ServerMessage = {
        messageType : MessageType.JOIN,
        message : {
          username : this.username
        }
      }
      this.bidService.sendMessage(serverMessage); 
    }

    messageReceived(data:ServerMessage) {
      console.log(data);
      switch (data.messageType) {
        case MessageType.JOIN : 
          if(data.message.username == this.username && data.message.success) {
            this.isJoined = true;
          }
        break;

        case MessageType.USER_LIST : 
          this.userObject = data.message.userList;
        break;

        case MessageType.BID : 
          this.bids = data.message.bidList;
          this.setHighestBid();
        break;

      }
    }

    setHighestBid() {
      if(this.bids.length > 0) {
        if(this.bids.length == 1) {
          this.highestBid = this.bids;
        }else{
          let maxIndex = 0;
          for(let i=0; i<this.bids.length; i++) {
            if(this.bids[i].bid > this.bids[maxIndex].bid) 
              maxIndex = i; 
          }
          console.log("maxindex" + maxIndex);
          this.highestBid.splice(0,1);
          this.highestBid.push(this.bids[maxIndex]);
        }
      }
    }

    getHighBidder() {
      if(this.highestBid.length > 0) {
        return this.highestBid[0].username;
      }
    }

    getHighBid() {
      if(this.highestBid.length > 0) {
        return this.highestBid[0].bid;
      }
    }
    
}
