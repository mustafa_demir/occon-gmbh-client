export interface ServerMessage {
    messageType : MessageType
    message : any;
}

export enum MessageType {
    JOIN,
    USER_LIST,
    BID
}